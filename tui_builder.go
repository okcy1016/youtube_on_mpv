package main


import (
	"github.com/marcusolsson/tui-go"
	"log"
)


// Declare ui globally for switch pages inside a function
var ui tui.UI


func ShowWelcomePage() {
	// Construct welcome page
	input_entry_0 := tui.NewEntry()
	input_entry_0.SetFocused(true)

	form := tui.NewGrid(0, 0)
	form.AppendRow(tui.NewLabel("Please search for something or type an url: "))
	form.AppendRow(input_entry_0)

	status := tui.NewStatusBar("Ready.")

	submit_button := tui.NewButton("[Submit]")
	submit_button.OnActivated(func(b *tui.Button) {
		status.SetText("Processing ...")
		SwitchToResultPage(input_entry_0.Text())
	})

	buttons := tui.NewHBox(
		tui.NewSpacer(),
		tui.NewPadder(1, 0, submit_button),
	)

	window := tui.NewVBox(
		tui.NewPadder(30, 1, tui.NewLabel(logo)),
		tui.NewPadder(12, 0, tui.NewLabel("Welcome to youtube_mpv, it's utility for play better with mpv for youtube.")),
		tui.NewPadder(1, 1, form),
		buttons,
	)
	window.SetBorder(true)

	wrapper := tui.NewVBox(
		tui.NewSpacer(),
		window,
		tui.NewSpacer(),
	)
	content := tui.NewHBox(tui.NewSpacer(), wrapper, tui.NewSpacer())
	
	// First page is welcome page
	welcome_page := tui.NewVBox(
		content,
		status,
	)

	tui.DefaultFocusChain.Set(input_entry_0, submit_button)

	// Prevent potential error while using "ui, err := ..."
	var err error
	ui, err = tui.New(welcome_page)
	if err != nil {
		log.Fatal(err)
	}

	ui.SetKeybinding("Ctrl+C", func() { ui.Quit() })
	ui.SetKeybinding("Enter", func() {
		status.SetText("Processing ...")
		SwitchToResultPage(input_entry_0.Text())
	})

	if err := ui.Run(); err != nil {
		log.Fatal(err)
	}
}


func SwitchToResultPage(keyword string) {
	// Clear last page's keybindings
	ui.ClearKeybindings()
	ui.SetKeybinding("Ctrl+C", func() { ui.Quit() })
	
	resultSlice, resultURLSlice := SearchOnYoutube(keyword)
	
	// Construct page
	// Construct list
	resultList := tui.NewList()
	resultList.AddItems(resultSlice...)
	resultList.SetFocused(true)
	resultList.SetSelected(0)

	// A shoody solution to prevent automatically start to play while detecting item activation by pressing enter once at the welcome page
	resultList.OnSelectionChanged(func (_ *tui.List) {
		resultList.OnItemActivated(func (_ *tui.List) {
			selectedURL := resultURLSlice[resultList.Selected()]
			PlayWithMPV(selectedURL)
		})
	})

	// Construct others
	hintLabel0 := tui.NewLabel("\nSelect using arrow keys.")
	resultWithBorder := tui.NewVBox(
		tui.NewSpacer(),
		resultList,
		hintLabel0,
		tui.NewSpacer(),
	)
	resultWithBorder.SetBorder(true)
	resultPage := tui.NewVBox(
		tui.NewSpacer(),
		resultWithBorder,
		tui.NewSpacer(),
	)

	// Switching page
	ui.SetWidget(resultPage)
}
