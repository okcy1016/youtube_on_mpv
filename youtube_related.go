package main

import (
	"os"
	"net/http"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/youtube/v3"
	"log"
	"fmt"
)


func SearchOnYoutube(keyword string) ([]string, []string) {
	// Set http proxy enviroment
	os.Setenv("http_proxy", "you_http_proxy")

	httpClient := &http.Client{
		Transport: &transport.APIKey{Key: developerKey},
	}

	service, err := youtube.New(httpClient)
	if err != nil {
		log.Fatalf("Error creating new YouTube client: %v", err)
	}

	// Make the API call to YouTube.
	call := service.Search.List("id,snippet").
		Q(keyword).
		MaxResults(25)
	response, err := call.Do()
	handleError(err, "")

	// Group video, channel, and playlist results in separate lists.
	videos := make(map[string]string)
	channels := make(map[string]string)
	playlists := make(map[string]string)

	// Iterate through each item and add it to the correct list.
	for _, item := range response.Items {
		switch item.Id.Kind {
		case "youtube#video":
			videos[item.Id.VideoId] = item.Snippet.Title
		case "youtube#channel":
			channels[item.Id.ChannelId] = item.Snippet.Title
		case "youtube#playlist":
			playlists[item.Id.PlaylistId] = item.Snippet.Title
		}
	}

	// Contruct result slices
	var resultSlice []string
	var resultURLSlice []string
	for id, title := range videos {
		resultSlice = append(resultSlice, fmt.Sprintf("[%v] %v\n", id, title))
		resultURLSlice = append(resultURLSlice, fmt.Sprintf("https://www.youtube.com/watch?v=%s", id))
	}

	return resultSlice, resultURLSlice
}
