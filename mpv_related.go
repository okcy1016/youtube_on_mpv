package main


import (
	"os/exec"
)


func PlayWithMPV(url_ string) {
	// Set up mpv player command
	mpvCMD := exec.Command("mpv", "--ytdl-raw-options=format=22", url_)

	// Get stdout
	// mpvCMDStdout, _ := mpvCMD.StdoutPipe()

	mpvCMD.Start()

	// Update output in tui in a go routine
	// go UpdateTextViewOutput(mpvCMDStdout, tuiTextView)

	mpvCMD.Wait()
}
